%% Formation control utilizing edge tension energy with a static, undirected

clear all; close all;
%% Set up Robotarium object

N = 7;
initial_conditions = generate_initial_conditions(N, 'Width', 2, 'Height', 1);
r = Robotarium('NumberOfRobots', N, 'ShowFigure', true, 'InitialConditions', initial_conditions);
controller = create_si_position_controller_no_limits();
error = 0;
sense_radius = 0.4; %meters
avoid_radius = 0.25; %meters
error_thresh = 0.05;
x=r.get_poses();
r.step();
%% Set up constants for experiment
%           1       2      
nodes = [-1.0,      1.0;   % x
         0.0,       0.0];  % y
%        1  2  
a_sch = [1;2];
    
idx = 1;

%Gains for the transformation from single-integrator to unicycle dynamics
formation_control_gain = 10;

% Select the number of iterations for the experiment.  This value is
% arbitrary
iterations = 5000;

% Communication topology for the desired formation.  We need 2 * N - 3 = 9
% edges to ensure that the formation is rigid

L = [3 -1 0 -1 0 -1; ... 
    -1 3 -1 0 -1 0; ... 
    0 -1 3 -1 0 -1; ... 
    -1 0 -1 3 -1 0; ... 
    0 -1 0 -1 3 -1; ... 
    -1 0 -1 0 -1 3];...
   
% The desired inter-agent distance for the formation
d = 0.5; 

% Pre-compute diagonal values for the rectangular formation
ddiag = sqrt((2*d)^2 + d^2);
d2 = sqrt(d^2+(.5*d)^2);
d3 = sqrt((2*d)^2+(0.5*d)^2);
% Weight matrix containing the desired inter-agent distances to achieve a
% rectuangular formation 
weights = [ 0 d 0 ddiag 0 d; ... 
            d 0 d 0 d 0; ... 
            0 d 0 d 0 ddiag; ... 
            ddiag 0 d 0 d 0; ... 
            0 d 0 d 0 d; ... 
            d 0 ddiag 0 d 0];...
            
% Initialize velocity vector for agents.  Each agent expects a 2 x 1
% velocity vector containing the linear and angular velocity, respectively.
dx = zeros(2, N);
dx_avoid = zeros(2, N-1);
A = zeros(N-1,N);
%% Grab tools for converting to single-integrator dynamics and ensuring safety 

% uni_barrier_cert = create_uni_barrier_certificate_with_boundary();
si_to_uni_dyn = create_si_to_uni_dynamics('LinearVelocityGain', 0.5, 'AngularVelocityLimit', pi/3);

% Iterate for the previously specified number of iterations
des_pos(1:2) = [-1;0];

%color for plotting
CM = rand(N,3);
CM(N,:) = [1,0,0];

%Marker, font, and line sizes
marker_size_goal = determine_marker_size(r, 0.20);
marker_size_robot = determine_robot_marker_size(r);
font_size = determine_font_size(r, 0.05);
line_width = 5;

for i = 1:N
    % Initialize additional information plot here. Note the order of
    % plotting matters, objects that are plotted later will appear over
    % object plotted previously.
    
    % Text for robot identification
    if i == N
        robot_caption = sprintf('Rogue Robot');
    else
        robot_caption = sprintf('Robot %d', i);
    end
    
    % Plot colored circles showing robot location.   
    if i == N
        g(i) = plot(x(1,i),x(2,i),'o','MarkerSize', .01,'LineWidth',1,'Color',CM(i,:));
    else
        g(i) = plot(x(1,i),x(2,i),'o','MarkerSize', sense_radius*100,'LineWidth',2,'Color',CM(i,:));
        g2(i) = plot(x(1,i),x(2,i),'o','MarkerSize', avoid_radius*100,'LineWidth',2,'Color',CM(N,:));
    end
    % Plot the robot label text 
    robot_labels{i} = text(500, 500, robot_caption, 'FontSize', font_size, 'FontWeight', 'bold');
end 
for t = 0:iterations
    
    
    
    % Retrieve the most recent poses from the Robotarium.  The time delay is
    % approximately 0.033 seconds
    x = r.get_poses();
    
    % Update Plotting Information and Locations
    
        for q = 1:N
            g(q).XData = x(1,q);
            g(q).YData = x(2,q);
           if q ~= N
                g2(q).XData = x(1,q);
                g2(q).YData = x(2,q);
           end
            
            robot_labels{q}.Position = x(1:2, q) + [-0.15;0.15];
            
        end
    %% Algorithm
    
    %This section contains the actual algorithm for formation control!
    
    %Calculate single integrator control inputs using edge-energy consensus
    
    for i = 1:N
        
        
        % Initialize velocity to zero for each agent.  This allows us to sum
        % over agent i's neighbors
        dx(:, i) = [0 ; 0];
        if i < N
        dx_avoid(:,i) = [0;0];
        end
        
        if i == N
            if error < error_thresh

                idx = idx + 1;

                if idx > size(a_sch(:,1))
                    idx = 1;
                end
                des_pos(1:2,1) = nodes(:,idx);
            end
                
            [dx(:,N), error] = controller(x(1:2, N), des_pos(1:2, 1));
        end
        
        % Get the topological neighbors of agent i from the communication
        % topology
        if i == N
            
        else
            dx(:,i) = [0;0];
            for j = topological_neighbors(L, i)

                % For each neighbor, calculate appropriate formation control term and
                % add it to the total velocity

                dx(:, i) = dx(:, i) + ...
                formation_control_gain*(norm(x(1:2, i) - x(1:2, j))^2 - weights(i, j)^2) ... 
                *(x(1:2, j) - x(1:2, i));
            end 

            for k = 1:N
                distance = norm(x(1:2, k) - x(1:2, i));
                if i == k

                else
                    if distance < avoid_radius
                        A(i,k) = 1;
                    end
                    if distance > sense_radius
                        A(i,k) = 0;
                    end
                    if A(i,k) == 1
                        weight = (distance - sense_radius)/distance;
                                                               
                        dx_avoid(:,i) = dx_avoid(:,i) + formation_control_gain*weight*(x(1:2,k)-x(1:2,i));
                    end
                end
                
            end
        dx(:,1) = [0;0];    
        dx = dx + [dx_avoid, [0;0]];
        
        end  

        marker_size_robot = num2cell(ones(1,N)*sense_radius*100);
        marker_size_robot{N} = .01;
        [g.MarkerSize] = marker_size_robot{:};
        font_size = determine_font_size(r, 0.05);
        iteration_label.FontSize = font_size;
        time_label.FontSize = font_size;

        for k = 1:N
            % Have to update font in loop for some conversion reasons.
            % Again this is unnecessary when submitting as the figure
            % window does not change size when deployed on the Robotarium.
            robot_labels{k}.FontSize = font_size;
            %goal_labels{k}.FontSize = font_size;
            robot_details_text{k}.FontSize = font_size;
        end
    end
    
    %% Avoid actuator errors
    
    % To avoid errors, we need to threshold dx
    norms = arrayfun(@(x) norm(dx(:, x)), 1:N);
    threshold = 3/4*r.max_linear_velocity;
    to_thresh = norms > threshold;
    dx(:, to_thresh) = threshold*dx(:, to_thresh)./norms(to_thresh);
    
    % Transform the single-integrator dynamics to unicycle dynamics using a provided utility function
    dx = si_to_uni_dyn(dx, x);  
    %dx(:,1:(N-1)) = uni_barrier_cert(dx(:,1:(N-1)), x(:,1:(N-1)));
    
%     norms = arrayfun(@(x) norm(dx(:, x)), 1:N);
%     threshold = 3/4*r.max_linear_velocity;
%     to_thresh = norms > threshold;
%     dx(:, to_thresh) = threshold*dx(:, to_thresh)./norms(to_thresh);
    % Set velocities of agents 1:N
    r.set_velocities(1:N, dx);
    
    % Send the previously set velocities to the agents.  This function must be called!
    r.step();   
end

% We can call this function to debug our experiment!  Fix all the errors
% before submitting to maximize the chance that your experiment runs
% successfully.
r.debug();

%% Helper Functions

% Marker Size Helper Function to scale size of markers for robots with figure window
% Input: robotarium class instance
function marker_size = determine_robot_marker_size(robotarium_instance)

% Get the size of the robotarium figure window in pixels
curunits = get(robotarium_instance.figure_handle, 'Units');
set(robotarium_instance.figure_handle, 'Units', 'Pixels');
cursize = get(robotarium_instance.figure_handle, 'Position');
set(robotarium_instance.figure_handle, 'Units', curunits);

% Determine the ratio of the robot size to the x-axis (the axis are
% normalized so you could do this with y and figure height as well).
robot_ratio = (robotarium_instance.robot_diameter + 0.03)/...
    (robotarium_instance.boundaries(2) - robotarium_instance.boundaries(1));

% Determine the marker size in points so it fits the window. cursize(3) is
% the width of the figure window in pixels. (the axis are
% normalized so you could do this with y and figure height as well).
marker_size = cursize(3) * robot_ratio;

end

% Marker Size Helper Function to scale size with figure window
% Input: robotarium instance, desired size of the marker in meters
function marker_size = determine_marker_size(robotarium_instance, marker_size_meters)

% Get the size of the robotarium figure window in pixels
curunits = get(robotarium_instance.figure_handle, 'Units');
set(robotarium_instance.figure_handle, 'Units', 'Pixels');
cursize = get(robotarium_instance.figure_handle, 'Position');
set(robotarium_instance.figure_handle, 'Units', curunits);

% Determine the ratio of the robot size to the x-axis (the axis are
% normalized so you could do this with y and figure height as well).
marker_ratio = (marker_size_meters)/(robotarium_instance.boundaries(2) -...
    robotarium_instance.boundaries(1));

% Determine the marker size in points so it fits the window. cursize(3) is
% the width of the figure window in pixels. (the axis are
% normalized so you could do this with y and figure height as well).
marker_size = cursize(3) * marker_ratio;

end

% Font Size Helper Function to scale size with figure window
% Input: robotarium instance, desired height of the font in meters
function font_size = determine_font_size(robotarium_instance, font_height_meters)

% Get the size of the robotarium figure window in point units
curunits = get(robotarium_instance.figure_handle, 'Units');
set(robotarium_instance.figure_handle, 'Units', 'Pixels');
cursize = get(robotarium_instance.figure_handle, 'Position');
set(robotarium_instance.figure_handle, 'Units', curunits);

% Determine the ratio of the font height to the y-axis
font_ratio = (font_height_meters)/(robotarium_instance.boundaries(4) -...
    robotarium_instance.boundaries(3));

% Determine the font size in points so it fits the window. cursize(4) is
% the hight of the figure window in points.
font_size = cursize(4) * font_ratio;

end
