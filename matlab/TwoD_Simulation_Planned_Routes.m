
close all, clear all

N = 4;
initial_positions = generate_initial_conditions(N, 'Spacing', 0.5);

initial_posotions = [-1.0,     0.0,    1.0,     1.0,    0.0;   % x
                      0.6,     0.6,    0.6,    -0.6,   -0.6;
                      0.0,     0.0,    0.0,     0.0,    0.0];
                  
r = Robotarium('NumberOfRobots', N, 'ShowFigure', true, 'InitialConditions', initial_positions);

sense_radius = 0.8; %meters
avoid_radius = 0.5; %meters

velocity_magnitude_limit = 0.13; %m/s

% Create a barrier certificate so that the robots don't collide
%si_barrier_certificate = create_uni_barrier_certificate_with_boundary('WheelVelocityLimit',13);
si_to_uni_dynamics = create_si_to_uni_dynamics('AngularVelocityLimit',pi/4);
 
                 
% |-----------------------|
% |  1        2         3 |
% |  7        x         8 |
% |  6        5         4 |
% |-----------------------|

%           1       2       3       4       5       6       7       8
nodes = [-1.0,      0,      1.0,    1.0,    0,      -1.0,    -1.0,  1.0 ;   % x
         0.6,       0.6,    0.6,    -0.6,   -0.6,   -0.6,    0.0,   -0.0];  % y

% Schedule of destination nodes for each agent
%        1  2  3, 4
a_sch = [1, 5, 3, 7;
         6, 2, 4, 8];
     
 

% Index for each agent to keep track of which node in their schedule they
% are headed to
idx = [1, 1, 1, 1];

        
% We'll make the rotation error huge so that the initialization checker
% doesn't care about it
args = {'PositionError', 0.02, 'RotationError', 50};
% init_checker = create_is_initialized(args{:});
controller = create_si_position_controller_no_limits();

% Get initial location data for while loop condition.
x=r.get_poses();
r.step();

%color for plotting
CM = rand(N,3);
CM(N,:) = [1,0,0];

des_pos = zeros([3, N]); % Initialize array of desired positions

%Marker, font, and line sizes
marker_size_goal = determine_marker_size(r, 0.20);
marker_size_robot = determine_robot_marker_size(r);
font_size = determine_font_size(r, 0.05);
line_width = 5;

start_time = tic; %The start time to compute time elapsed.

for i = 1:N
    % Initialize additional information plot here. Note the order of
    % plotting matters, objects that are plotted later will appear over
    % object plotted previously.
    
    % Text for robot identification
    if i == N
        robot_caption = sprintf('Rogue Robot');
    else
        robot_caption = sprintf('Robot %d', i);
    end
    % Text with goal identification
    goal_caption = sprintf('G%d', i);
    % Plot colored square for goal location.
    d(i) = plot(des_pos(1,i), des_pos(2,i),'s','MarkerSize',marker_size_goal,'LineWidth',line_width,'Color',CM(i,:));
    
    % Plot the goal identification text inside the goal location
    goal_labels{i} = text(des_pos(1,i)-0.05, des_pos(2,i), goal_caption, 'FontSize', font_size, 'FontWeight', 'bold');
    % Plot colored circles showing robot location.   
    if i == N
        g(i) = plot(x(1,i),x(2,i),'o','MarkerSize', marker_size_robot,'LineWidth',8,'Color',CM(i,:));
    else
        g(i) = plot(x(1,i),x(2,i),'o','MarkerSize', sense_radius*100,'LineWidth',2,'Color',CM(i,:));
        g2(i) = plot(x(1,i),x(2,i),'o','MarkerSize', avoid_radius*100,'LineWidth',5,'Color',CM(N,:));
    end
    
    % Plot the robot label text 
    robot_labels{i} = text(500, 500, robot_caption, 'FontSize', font_size, 'FontWeight', 'bold');
    
end

% Plot the iteration and time in the lower left. Note when run on your 
% computer, this time is based on your computers simulation time. For a
% better approximation of experiment time on the Robotarium when running
% this simulation on your computer, multiply iteration by 0.033. 
iteration_caption = sprintf('Iteration %d', 0);
time_caption = sprintf('Total Time Elapsed %0.2f', toc(start_time));

iteration_label = text(-1.5, -0.8, iteration_caption, 'FontSize', font_size, 'Color', 'r', 'FontWeight', 'bold');
time_label = text(-1.5, -0.9, time_caption, 'FontSize', font_size, 'Color', 'r', 'FontWeight', 'bold');

% Import and scale the Bell logo appropriately.
gt_img = imread('Bell.jpg'); % Original input image file

% Display the image with an associated spatial referencing object.
x_img = linspace(-1.0, 1.0, size(gt_img,2));
y_img = linspace(1.0, -1.0, size(gt_img,1)); %Note the 1 to -1 here due to the (1,1) pixel being in the top left corner.
gt_img_handle = image(x_img, y_img, gt_img,'CDataMapping','scaled');

% We can change the order of plotting priority, we will plot goals on the 
% bottom and have the iteration/time caption on top.
%uistack(a,'bottom'); %Arrows are at the very bottom.
uistack([goal_labels{:}], 'bottom'); % Goal labels are at the very bottom, arrows are now only above the goal labels.
uistack(d, 'bottom');% Goal squares are at the very bottom, goal labels are above the squares and goal arrows are above those.
uistack([iteration_label], 'top'); % Iteration label is on top.
uistack([time_label], 'top'); % Time label is above iteration label.
uistack(gt_img_handle, 'bottom'); % Image under everything else.

i = 1;
while i < N+1
    
    des_pos(1:2,i) = nodes(:,a_sch(idx(i),i)); % Initial destination for each agent
    i = i+1;
    
end

A = zeros(N); %Initialize adjacency matrix
weights = zeros(N);

errors = zeros(N,1);
error_thresh = 0.02; %Defines how close an agent must get to a node before navigating to next node
iter = 5000;
j = 0;
steps = 0;
while(steps < iter)

    x = r.get_poses();
    % Update Plotting Information and Locations
        for q = 1:N
            g(q).XData = x(1,q);
            g(q).YData = x(2,q);
           if q ~= N
                g2(q).XData = x(1,q);
                g2(q).YData = x(2,q);
           end
            
            robot_labels{q}.Position = x(1:2, q) + [-0.15;0.15];
            
        end
        
    i = 1; 
    while i < N+1
        if errors(i) < error_thresh
            
            idx(i) = idx(i )+ 1;
            
            if idx(i) > size(a_sch(:,1))
                idx(i) = 1;
            end
            
            des_pos(1:2,i) = nodes(:,a_sch(idx(i),i)); 
            
            for j = 1:N
                goal_labels{j}.Position = des_pos(1:2, j)-[0.05;0];

                d(j).XData = des_pos(1,j);
                d(j).YData = des_pos(2,j);

            end
            
        end
            
        i = i+1;
    end
    
    %Use a standard position controller to drive the agents towards their
    %desired node.
    [dxi_nav, errors] = controller(x(1:2, :), des_pos(1:2, :));
    
    i = 1;
    while i < N+1 %loop through agents
        j = 1;
        while j < N+1 % cycle through agents
            
            distance = norm(x(1:2, i) - x(1:2, j));
            if A(i,j) == 1  %if Connected
                if distance > sense_radius %if agents move outside of sense radius, remove edge
                    A(i,j) = 0;
                end
            end
            if A(i,j) == 0
                if distance < avoid_radius %activate edge if agent comes within avoid radius
                    A(i,j) = 1;
                end
            end
            if i == j %agent is not connected with itself                
                A(i,j) = 0;               
            end
            if i ~= j
                weights(i,j) = (distance - sense_radius)/distance; %calculate collision avoid weight
            else
                weights(i,j) = 0;
            end
            j = j + 1;
        end
        i = i+1;
               
    end
   
    dxi_avoid = zeros(2,N-1); %reset avoidance contribution to velocity
    
    i = 1;
    while i < N %loop through agents that aren't rogue
        j = 1;
        while j < N+1
            dxi_avoid(1:2,i) = dxi_avoid(1:2,i) + 0.28 * A(i,j)*weights(i,j)*(x(1:2,j) - x(1:2,i));
            j = j + 1;
        end
        i = i + 1;
    end
    
    dxi_avoid(1:2,N) = [0;0];
    
    dxi = dxi_nav + dxi_avoid;   
    
    norms = arrayfun(@(idx) norm(dxi(:, idx)), 1:N);
    
    to_normalize_avoid = norms > velocity_magnitude_limit;
        
    if(~isempty(norms(to_normalize_avoid)))                        
        dxi_avoid(:, to_normalize_avoid) = velocity_magnitude_limit*dxi(:, to_normalize_avoid)./norms(to_normalize_avoid);
    end
    
    
    to_normalize = norms > velocity_magnitude_limit;
        
    if(~isempty(norms(to_normalize)))                        
        dxi(:, to_normalize) = velocity_magnitude_limit*dxi(:, to_normalize)./norms(to_normalize);
    end


    %dxi = si_barrier_certificate(dxi, x);      
    dxu = si_to_uni_dynamics(dxi, x);

    r.set_velocities(1:N, dxu);
    r.step();   
    steps = steps+1;
    % Update Iteration and Time marker
    iteration_caption = sprintf('Iteration %d', i);
    time_caption = sprintf('Total Time Elapsed %0.2f', toc(start_time));
    
    iteration_label.String = iteration_caption;
    time_label.String = time_caption;
    
    % Resize Marker Sizes (In case user changes simulated figure window
    % size, this is unnecessary in submission as the figure window 
    % does not change size).

    marker_size_goal = num2cell(ones(1,N)*determine_marker_size(r, 0.20));
    [d.MarkerSize] = marker_size_goal{:};
    marker_size_robot = num2cell(ones(1,N)*sense_radius*100);
    marker_size_robot{4} = 20;
    [g.MarkerSize] = marker_size_robot{:};
    font_size = determine_font_size(r, 0.05);
    iteration_label.FontSize = font_size;
    time_label.FontSize = font_size;
    
    for k = 1:N
        % Have to update font in loop for some conversion reasons.
        % Again this is unnecessary when submitting as the figure
        % window does not change size when deployed on the Robotarium.
        robot_labels{k}.FontSize = font_size;
        goal_labels{k}.FontSize = font_size;
        robot_details_text{k}.FontSize = font_size;
    end
end

% We can call this function to debug our experiment!  Fix all the errors
% before submitting to maximize the chance that your experiment runs
% successfully.
r.debug();

%% Helper Functions

% Marker Size Helper Function to scale size of markers for robots with figure window
% Input: robotarium class instance
function marker_size = determine_robot_marker_size(robotarium_instance)

% Get the size of the robotarium figure window in pixels
curunits = get(robotarium_instance.figure_handle, 'Units');
set(robotarium_instance.figure_handle, 'Units', 'Pixels');
cursize = get(robotarium_instance.figure_handle, 'Position');
set(robotarium_instance.figure_handle, 'Units', curunits);

% Determine the ratio of the robot size to the x-axis (the axis are
% normalized so you could do this with y and figure height as well).
robot_ratio = (robotarium_instance.robot_diameter + 0.03)/...
    (robotarium_instance.boundaries(2) - robotarium_instance.boundaries(1));

% Determine the marker size in points so it fits the window. cursize(3) is
% the width of the figure window in pixels. (the axis are
% normalized so you could do this with y and figure height as well).
marker_size = cursize(3) * robot_ratio;

end

% Marker Size Helper Function to scale size with figure window
% Input: robotarium instance, desired size of the marker in meters
function marker_size = determine_marker_size(robotarium_instance, marker_size_meters)

% Get the size of the robotarium figure window in pixels
curunits = get(robotarium_instance.figure_handle, 'Units');
set(robotarium_instance.figure_handle, 'Units', 'Pixels');
cursize = get(robotarium_instance.figure_handle, 'Position');
set(robotarium_instance.figure_handle, 'Units', curunits);

% Determine the ratio of the robot size to the x-axis (the axis are
% normalized so you could do this with y and figure height as well).
marker_ratio = (marker_size_meters)/(robotarium_instance.boundaries(2) -...
    robotarium_instance.boundaries(1));

% Determine the marker size in points so it fits the window. cursize(3) is
% the width of the figure window in pixels. (the axis are
% normalized so you could do this with y and figure height as well).
marker_size = cursize(3) * marker_ratio;

end

% Font Size Helper Function to scale size with figure window
% Input: robotarium instance, desired height of the font in meters
function font_size = determine_font_size(robotarium_instance, font_height_meters)

% Get the size of the robotarium figure window in point units
curunits = get(robotarium_instance.figure_handle, 'Units');
set(robotarium_instance.figure_handle, 'Units', 'Pixels');
cursize = get(robotarium_instance.figure_handle, 'Position');
set(robotarium_instance.figure_handle, 'Units', curunits);

% Determine the ratio of the font height to the y-axis
font_ratio = (font_height_meters)/(robotarium_instance.boundaries(4) -...
    robotarium_instance.boundaries(3));

% Determine the font size in points so it fits the window. cursize(4) is
% the hight of the figure window in points.
font_size = cursize(4) * font_ratio;

end

