% Initializing the agents to random positions with barrier certificates. 
% This script shows how to initialize robots to a particular point.
% Sean Wilson
% 07/2019

N = 4;
initial_positions = generate_initial_conditions(N, 'Spacing', 0.5);
r = Robotarium('NumberOfRobots', N, 'ShowFigure', true, 'InitialConditions', initial_positions);

% Create a barrier certificate so that the robots don't collide
%si_barrier_certificate = create_si_barrier_certificate();
si_to_uni_dynamics = create_si_to_uni_dynamics();
        
%Get randomized initial conditions in the robotarium arena
final_goal_points = generate_initial_conditions(N, ...
    'Width', r.boundaries(2)-r.boundaries(1)-r.robot_diameter, ...
    'Height', r.boundaries(4)-r.boundaries(3)-r.robot_diameter, ...
    'Spacing', 0.5);
final_goal_points = [-1.5, -0, 1, 1.5;
                     -1, -0.5, 0.5, 1;
                     0 , 0, 0, 0];
                 
% |-----------------------|
% |  1        2         3 |
% |  7        x         8 |
% |  6        5         4 |
% |-----------------------|

%           1       2       3       4       5       6       7       8
nodes = [-1.0,      0,      1.0,    1.0,    0,      -1.0,    -1.0,  1.0 ;   % x
         0.8,       0.8,    0.8,    -0.8,   -0.8,   -0.8,    0.0,   0.0];  % y
%        1  2  3
a_sch = [1, 5, 3, 7;
         6, 2, 4, 8];
     
% rougue_sch = [7, 8]';
     
 

% Index for each agent to keep track of which node in their schedule they
% are headed to
idx = [1, 1, 1, 1];

        
% We'll make the rotation error huge so that the initialization checker
% doesn't care about it
args = {'PositionError', 0.02, 'RotationError', 50};
% init_checker = create_is_initialized(args{:});
controller = create_si_position_controller();

% Get initial location data for while loop condition.
x=r.get_poses();
r.step();

des_pos = zeros([3, N]); % Destination nodes for each agent

       
error_thresh = 0.01; % 1cm
iter = 10000;
j = 0;
while(j < iter)

    x = r.get_poses();
    
    
    [dxi, errors] = controller(x(1:2, :), des_pos(1:2, :));
    
    i = 1; 
    while i < N+1
        if errors(i) < error_thresh
            if idx(i)== 1
                idx(i) = 2;
            else
                idx(i) = mod(idx(i)+1,2);
            end
            des_pos(1:2,i) = nodes(:,a_sch(idx(i),i)); 
        end
            
        i = i+1;
    end
%     disp(errors)
    % dxi = si_barrier_certificate(dxi, x(1:2, :));      
    dxu = si_to_uni_dynamics(dxi, x);

    r.set_velocities(1:N, dxu);
    r.step();   
    j = j+1;
end

% We can call this function to debug our experiment!  Fix all the errors
% before submitting to maximize the chance that your experiment runs
% successfully.
r.debug();

